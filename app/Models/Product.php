<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 *
 * @property int $id
 * @property int|null $category_id
 * @property string|null $name
 * @property string|null $introduction
 * @property int|null $price
 * @property string|null $location
 * @property string|null $description
 * @property Carbon|null $created_at
 * @property int|null $created_by
 * @property Carbon|null $updated_at
 * @property int|null $updated_by
 *
 * @property Category $category
 * @property AdminUser $admin_user
 * @property Collection|ProductImage[] $product_images
 *
 * @package App\Models
 */
class Product extends Model
{
	protected $table = 'products';

	protected $casts = [
		'category_id' => 'int',
		'price' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int'
	];

	protected $fillable = [
		'category_id',
		'name',
		'introduction',
		'price',
		'location',
		'description',
		'created_by',
		'updated_by'
	];

	public function category()
	{
		return $this->belongsTo(Category::class);
	}

    public function admin_user()
    {
        return $this->belongsTo(AdminUser::class, 'updated_by');
    }

	public function product_images()
	{
		return $this->hasMany(ProductImage::class);
	}
}
