<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ProjectImage
 *
 * @property int $id
 * @property int $project_id
 * @property string $path
 * @property bool|null $is_main_image
 * @property bool|null $is_thumbnail
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property Project $project
 *
 * @package App\Models
 */
class ProjectImage extends Model
{
	protected $table = 'project_images';

	protected $casts = [
		'project_id' => 'int',
		'is_main_image' => 'bool',
		'is_thumbnail' => 'bool'
	];

	protected $fillable = [
		'project_id',
		'path',
		'is_main_image',
		'is_thumbnail'
	];

    public function project()
    {
        return $this->belongsTo(Project::class);
    }
}
