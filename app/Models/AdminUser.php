<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class AdminUser
 *
 * @property int $id
 * @property int|null $is_super_admin
 * @property int $role_id
 * @property string|null $first_name
 * @property string|null $last_name
 * @property string $email
 * @property string $password
 * @property string|null $language
 * @property int|null $status_id
 * @property string|null $image_path
 * @property string|null $remember_token
 * @property Carbon|null $email_verified_at
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property Status $status
 * @property Role $role
// * @property Collection|Project[] $projectss
 *
 * @package App\Models
 */
class AdminUser extends Authenticatable
{
//	protected $table = 'admin_users';

	protected $casts = [
		'is_super_admin' => 'int',
		'role_id' => 'int',
		'status_id' => 'int'
	];

	protected $dates = [
		'email_verified_at'
	];

	protected $hidden = [
		'password',
		'remember_token'
	];

	protected $fillable = [
		'is_super_admin',
		'role_id',
		'first_name',
		'last_name',
		'email',
		'password',
		'language',
		'status_id',
		'image_path',
		'remember_token',
		'email_verified_at'
	];

	public function status()
	{
		return $this->belongsTo(Status::class);
	}

	public function role()
	{
		return $this->belongsTo(Role::class);
	}

	public function projects()
	{
		return $this->hasMany(Project::class, 'updated_by');
	}
}
