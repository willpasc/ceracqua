<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ProjectImage
 *
 * @property int $id
 * @property int $project_id
 * @property string $path
 * @property bool|null $is_main_image
 * @property bool|null $is_thumbnail
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property Product $product
 *
 * @package App\Models
 */
class ProductImage extends Model
{
	protected $table = 'product_images';

	protected $casts = [
		'product_id' => 'int',
		'is_main_image' => 'bool',
		'is_thumbnail' => 'bool'
	];

	protected $fillable = [
		'product_id',
		'path',
		'is_main_image',
		'is_thumbnail'
	];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
