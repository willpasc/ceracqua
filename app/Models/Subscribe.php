<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Subscribe
 * 
 * @property int $id
 * @property string $email
 * @property Carbon|null $created_at
 *
 * @package App\Models
 */
class Subscribe extends Model
{
	protected $table = 'subscribes';
	public $timestamps = false;

	protected $fillable = [
		'email'
	];
}
