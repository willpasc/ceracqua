<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ContactMessage
 * 
 * @property int $id
 * @property string|null $name
 * @property string|null $email
 * @property string|null $subject
 * @property string|null $phone
 * @property string|null $address
 * @property string|null $message
 * @property Carbon|null $created_at
 *
 * @package App\Models
 */
class ContactMessage extends Model
{
	protected $table = 'contact_messages';
	public $timestamps = false;

	protected $fillable = [
		'name',
		'email',
		'subject',
		'phone',
		'address',
		'message'
	];
}
