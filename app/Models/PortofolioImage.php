<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PortofolioImage
 *
 * @property int $id
 * @property int $portofolio_id
 * @property string $path
 * @property bool|null $is_main_image
 * @property bool|null $is_thumbnail
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property Portofolio $portofolio
 *
 * @package App\Models
 */
class PortofolioImage extends Model
{
	protected $table = 'portofolio_images';

	protected $casts = [
		'project_id' => 'int',
		'is_main_image' => 'bool',
		'is_thumbnail' => 'bool'
	];

	protected $fillable = [
		'portofolio_id',
		'path',
		'is_main_image',
		'is_thumbnail'
	];

	public function portofolio()
	{
		return $this->belongsTo(Portofolio::class);
	}
}
