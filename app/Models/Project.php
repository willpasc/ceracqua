<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Project
 *
 * @property int $id
 * @property int|null $category_id
 * @property string|null $name
 * @property string|null $introduction
 * @property string|null $brief
 * @property string|null $location
 * @property string|null $description
 * @property Carbon|null $created_at
 * @property int|null $created_by
 * @property Carbon|null $updated_at
 * @property int|null $updated_by
 *
 * @property Category $category
 * @property AdminUser $admin_user
 * @property Collection|ProjectImage[] $project_images
 *
 * @package App\Models
 */
class Project extends Model
{
	protected $table = 'projects';

	protected $casts = [
		'category_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int'
	];

	protected $fillable = [
		'category_id',
		'name',
		'introduction',
		'brief',
		'location',
		'description',
		'created_by',
		'updated_by'
	];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function admin_user()
    {
        return $this->belongsTo(AdminUser::class, 'updated_by');
    }

    public function project_images()
    {
        return $this->hasMany(ProjectImage::class);
    }
}
