<?php
/**
 * Created by PhpStorm.
 * User: YANSEN
 * Date: 12/10/2018
 * Time: 10:06
 */

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\libs\Utilities;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductImage;
use App\Transformer\ProductTransformer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Yajra\DataTables\DataTables;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function getIndex(Request $request){
        $users = Product::all();
        return DataTables::of($users)
            ->setTransformer(new ProductTransformer())
            ->addIndexColumn()
            ->make(true);
    }

    public function index()
    {
        return view('admin.product.index');
    }

    public function show(Product $item)
    {
        $images = ProductImage::where('product_id', $item->id)->orderby('is_main_image','desc')->get();
        $productCategory = Category::where('product_id', $item->id)->first();

        $data = [
            'product'    => $item,
            'productCategory'    => $productCategory,
            'images'    => $images,
        ];
        return view('admin.product.show')->with($data);
    }

    public function create()
    {
        $product = Product::find(1);
        $categories = Category::all();

        $data = [
            'categories'    => $categories,
            'product'    => $product,
        ];
        return view('admin.product.index')->with($data);
    }

    public function store(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'name'        => 'required',
            ]);

            if ($validator->fails())
                return redirect()->back()->withErrors($validator->errors())->withInput($request->all());

            if(!$request->hasFile('main_image')){
                return back()->withErrors("Gambar Utama wajib diunggah!")->withInput($request->all());
            }

            $mainImages = $request->file('main_image');

            $dateTimeNow = Carbon::now('Asia/Jakarta');

            $newProduct = Product::create([
                'name'          => $request->input('name'),
                'category_id'   => $request->input('category'),
                'introduction'  => $request->input('introduction') ?? '',
                'price'         => $request->input('price') ?? '',
                'description'   => $request->input('description') ?? '',
                'status'        => 1,
                'created_at'    => $dateTimeNow->toDateTimeString(),
                'updated_at'    => $dateTimeNow->toDateTimeString(),
            ]);

            // save product main image, thumbnail and image detail
            //main image
            $img = Image::make($mainImages);
            $extStr = $img->mime();
            $ext = explode('/', $extStr, 2);
            $filename = $newProduct->id.'_main_'.Carbon::now('Asia/Jakarta')->format('Ymdhms'). '.'. $ext[1];

            $img->save(public_path('storage/products/'. $filename), 75);

            ProductImage::create([
                'product_id' => $newProduct->id,
                'path' => $filename,
                'is_main_image' => 1,
                'is_thumbnail' => 0,
            ]);


            //image detail
            if($request->hasFile('detail_image')){
                $detailImages = $request->file('detail_image');
                for($i = 0; $i < sizeof($detailImages); $i++){
                    $img = Image::make($detailImages[$i]);
                    $extStr = $img->mime();
                    $ext = explode('/', $extStr, 2);

                    $filename = $newProduct->id.'_'.$i.'_'.Carbon::now('Asia/Jakarta')->format('Ymdhms'). '.'. $ext[1];

                    $img->save(public_path('storage/products/'. $filename), 75);

                    ProductImage::create([
                        'product_id' => $newProduct->id,
                        'path' => $filename,
                        'is_main_image' => 0,
                        'is_thumbnail' => 0,
                    ]);
                }
            }

            Session::flash('success', 'Sukses membuat Product baru!');
            return redirect()->route('admin.product.edit',['item' => $newProduct->id]);

        }catch(\Exception $ex){
            error_log($ex);
            Log::error("Admin/ProductController store error: ". $ex);
            return back()->withErrors("Something Went Wrong")->withInput();
        }
    }

    public function edit(Product $item)
    {
        try{
            $categories = Category::all();
            $mainImage = ProductImage::where('product_id', $item->id)->where('is_main_image', 1)->first();
            $detailImage = ProductImage::where('product_id', $item->id)
                ->where('is_main_image', 0)
                ->where('is_thumbnail', 0)->get();
            $data = [
                'product'    => $item,
                'categories'    => $categories,
                'mainImage'    => $mainImage,
                'detailImage'    => $detailImage,
            ];
            return view('admin.product.edit')->with($data);

        }catch(\Exception $ex){
            Log::error("Admin/ProductController edit error: ". $ex);
            dd($ex);
        }
    }

    public function update(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'name'        => 'required',
            ]);

            if ($validator->fails())
                return redirect()->back()->withErrors($validator->errors())->withInput($request->all());

            $product = Product::find($request->input('id'));

            $dateTimeNow = Carbon::now('Asia/Jakarta');

            $product->name = $request->input('name');
            $product->category_id = $request->input('category');
            $product->introduction = $request->input('introduction') ?? '';
            $product->price = $request->input('price') ?? '';
            $product->description = $request->input('description') ?? '';
            $product->updated_at = $dateTimeNow->toDateTimeString();

            $product->save();

            // update product main image, thumbnail and image detail
            if($request->hasFile('main_image')){
                $mainImages = $request->file('main_image');
                $mainImage = ProductImage::where('product_id', $product->id)->where('is_main_image', 1)->first();
                if(!empty($mainImage)){
                    // Delete old image
                    if(!empty($mainImage->path)){
                        $oldPath = asset('storage/products/'. $mainImage->path);
                        if(file_exists($oldPath)) unlink($oldPath);
                    }

                    $mainImage->delete();
                }

                $img = Image::make($mainImages);
                $extStr = $img->mime();
                $ext = explode('/', $extStr, 2);
                $filename = $product->id.'_main_'.Carbon::now('Asia/Jakarta')->format('Ymdhms'). '.'. $ext[1];

                $img->save(public_path('storage/products/'. $filename), 75);

                ProductImage::create([
                    'product_id' => $product->id,
                    'path' => $filename,
                    'is_main_image' => 1,
                    'is_thumbnail' => 0,
                ]);
            }
            if($request->hasFile('detail_image')){
                $changedDetailImages = $request->file('detail_image');
                $detailImages = ProductImage::where('product_id', $product->id)->where('is_main_image', 0)->where('is_thumbnail', 0)->get();

                // Delete old image
                foreach ($detailImages as $oldImage){
                    if(!empty($oldImage->path)){
                        $oldPath = asset('storage/products/'. $oldImage->path);
                        if(file_exists($oldPath)) unlink($oldPath);
                    }
                    $oldImage->delete();
                }

                for($i = 0; $i < sizeof($changedDetailImages); $i++){
                    $img = Image::make($changedDetailImages[$i]);
                    $extStr = $img->mime();
                    $ext = explode('/', $extStr, 2);

                    $filename = $product->id.'_'.$i.'_'.Carbon::now('Asia/Jakarta')->format('Ymdhms'). '.'. $ext[1];

                    $img->save(public_path('storage/products/'. $filename), 75);

                    ProductImage::create([
                        'product_id' => $product->id,
                        'path'          => $filename,
                        'is_main_image' => 0,
                        'is_thumbnail'  => 0,
                    ]);
                }
            }

            Session::flash('success', 'Sukses mengubah data Product!');
            return redirect()->route('admin.product.index',['item' => $product->id]);

        }catch(\Exception $ex){
            error_log($ex);
            Log::error("Admin/ProductController update error: ". $ex);
            return back()->withErrors("Something Went Wrong")->withInput();
        }
    }

    public function getProducts(Request $request){
        $term = trim($request->q);
        $roles = Product::where('id', '!=', $request->id)
            ->where(function ($q) use ($term) {
                $q->where('name', 'LIKE', '%' . $term . '%');
            })
            ->get();

        $formatted_tags = [];

        foreach ($roles as $role) {
            $formatted_tags[] = ['id' => $role->id, 'text' => $role->name];
        }

        return \Response::json($formatted_tags);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        try {
            $deletedId = $request->input('id');
            $product = Product::find($deletedId);
            if(empty($product)){
                return Response::json(array('errors' => 'INVALID'));
            }

            $detailImages = ProductImage::where('product_id', $deletedId)->get();

            // Delete old image
            foreach ($detailImages as $oldImage){
                if(!empty($oldImage->path)){
                    $oldPath = asset('storage/products/'. $oldImage->path);
                    if(file_exists($oldPath)) unlink($oldPath);
                }
                $oldImage->delete();
            }

            $product->delete();

            Session::flash('success', 'Sukses menghapus Product!');
            return Response::json(array('success' => 'VALID'));
        }
        catch(\Exception $ex){
            error_log($ex);
            Log::error("Admin/ProductController destroy error: ". $ex);
            return Response::json(array('errors' => 'INVALID'));
        }


    }
}
