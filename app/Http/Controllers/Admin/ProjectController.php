<?php
/**
 * Created by PhpStorm.
 * User: YANSEN
 * Date: 12/10/2018
 * Time: 10:06
 */

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\libs\Utilities;
use App\Models\Category;
use App\Models\Project;
use App\Models\ProjectImage;
use App\Transformer\ProjectTransformer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Yajra\DataTables\DataTables;

class ProjectController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function getIndex(Request $request){
        $users = Project::all();
        return DataTables::of($users)
            ->setTransformer(new ProjectTransformer())
            ->addIndexColumn()
            ->make(true);
    }

    public function index()
    {
        return view('admin.project.index');
    }

    public function show(Project $item)
    {
        $images = ProjectImage::where('project_id', $item->id)->orderby('is_main_image','desc')->get();
        $productCategory = Category::where('project_id', $item->id)->first();

        $data = [
            'product'    => $item,
            'productCategory'    => $productCategory,
            'images'    => $images,
        ];
        return view('admin.project.show')->with($data);
    }

    public function create()
    {
        $project = Project::find(1);
        $categories = Category::all();

        $data = [
            'categories'    => $categories,
            'product'    => $project,
        ];
        return view('admin.project.create')->with($data);
    }

    public function store(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'name'        => 'required',
            ]);

            if ($validator->fails())
                return redirect()->back()->withErrors($validator->errors())->withInput($request->all());

            if(!$request->hasFile('main_image')){
                return back()->withErrors("Gambar Utama wajib diunggah!")->withInput($request->all());
            }

            $mainImages = $request->file('main_image');

            $dateTimeNow = Carbon::now('Asia/Jakarta');

            $newProject = Project::create([
                'name'          => $request->input('name'),
                'introduction'  => $request->input('introduction') ?? '',
                'brief'         => $request->input('brief') ?? '',
                'description'   => $request->input('description') ?? '',
                'status'        => 1,
                'created_at'    => $dateTimeNow->toDateTimeString(),
                'updated_at'    => $dateTimeNow->toDateTimeString(),
            ]);

            // save product main image, thumbnail and image detail
            //main image
            $img = Image::make($mainImages);
            $extStr = $img->mime();
            $ext = explode('/', $extStr, 2);
            $filename = $newProject->id.'_main_'.Carbon::now('Asia/Jakarta')->format('Ymdhms'). '.'. $ext[1];

            $img->save(public_path('storage/projects/'. $filename), 75);

            ProjectImage::create([
                'project_id' => $newProject->id,
                'path' => $filename,
                'is_main_image' => 1,
                'is_thumbnail' => 0,
            ]);


            //image detail
            if($request->hasFile('detail_image')){
                $detailImages = $request->file('detail_image');
                for($i = 0; $i < sizeof($detailImages); $i++){
                    $img = Image::make($detailImages[$i]);
                    $extStr = $img->mime();
                    $ext = explode('/', $extStr, 2);

                    $filename = $newProject->id.'_'.$i.'_'.Carbon::now('Asia/Jakarta')->format('Ymdhms'). '.'. $ext[1];

                    $img->save(public_path('storage/projects/'. $filename), 75);

                    ProjectImage::create([
                        'project_id' => $newProject->id,
                        'path' => $filename,
                        'is_main_image' => 0,
                        'is_thumbnail' => 0,
                    ]);
                }
            }

            Session::flash('success', 'Sukses membuat Project baru!');
            return redirect()->route('admin.project.create',['item' => $newProject->id]);

        }catch(\Exception $ex){
            error_log($ex);
            Log::error("Admin/ProjectController store error: ". $ex);
            return back()->withErrors("Something Went Wrong")->withInput();
        }
    }

    public function edit(Project $item)
    {
        try{
            $categories = Category::all();
            $mainImage = ProjectImage::where('project_id', $item->id)->where('is_main_image', 1)->first();
            $detailImage = ProjectImage::where('project_id', $item->id)
                ->where('is_main_image', 0)
                ->where('is_thumbnail', 0)->get();
            $data = [
                'project'    => $item,
                'categories'    => $categories,
                'mainImage'    => $mainImage,
                'detailImage'    => $detailImage,
            ];
            return view('admin.project.edit')->with($data);

        }catch(\Exception $ex){
            Log::error("Admin/ProjectController edit error: ". $ex);
            dd($ex);
        }
    }

    public function update(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'name'        => 'required',
            ]);

            if ($validator->fails())
                return redirect()->back()->withErrors($validator->errors())->withInput($request->all());

            $project = Project::find($request->input('id'));

            $dateTimeNow = Carbon::now('Asia/Jakarta');

            $project->name = $request->input('name');
            $project->introduction = $request->input('introduction') ?? '';
            $project->brief = $request->input('brief') ?? '';
            $project->description = $request->input('description') ?? '';
            $project->updated_at = $dateTimeNow->toDateTimeString();

            $project->save();

            // update product main image, thumbnail and image detail
            if($request->hasFile('main_image')){
                $mainImages = $request->file('main_image');
                $mainImage = ProjectImage::where('project_id', $project->id)->where('is_main_image', 1)->first();
                if(!empty($mainImage)){
                    // Delete old image
                    if(!empty($mainImage->path)){
                        $oldPath = asset('storage/projects/'. $mainImage->path);
                        if(file_exists($oldPath)) unlink($oldPath);
                    }

                    $mainImage->delete();
                }

                $img = Image::make($mainImages);
                $extStr = $img->mime();
                $ext = explode('/', $extStr, 2);
                $filename = $project->id.'_main_'.Carbon::now('Asia/Jakarta')->format('Ymdhms'). '.'. $ext[1];

                $img->save(public_path('storage/projects/'. $filename), 75);

                ProjectImage::create([
                    'project_id' => $project->id,
                    'path' => $filename,
                    'is_main_image' => 1,
                    'is_thumbnail' => 0,
                ]);
            }
            if($request->hasFile('detail_image')){
                $changedDetailImages = $request->file('detail_image');
                $detailImages = ProjectImage::where('project_id', $project->id)->where('is_main_image', 0)->where('is_thumbnail', 0)->get();

                // Delete old image
                foreach ($detailImages as $oldImage){
                    if(!empty($oldImage->path)){
                        $oldPath = asset('storage/projects/'. $oldImage->path);
                        if(file_exists($oldPath)) unlink($oldPath);
                    }
                    $oldImage->delete();
                }

                for($i = 0; $i < sizeof($changedDetailImages); $i++){
                    $img = Image::make($changedDetailImages[$i]);
                    $extStr = $img->mime();
                    $ext = explode('/', $extStr, 2);

                    $filename = $project->id.'_'.$i.'_'.Carbon::now('Asia/Jakarta')->format('Ymdhms'). '.'. $ext[1];

                    $img->save(public_path('storage/projects/'. $filename), 75);

                    ProjectImage::create([
                        'project_id' => $project->id,
                        'path'          => $filename,
                        'is_main_image' => 0,
                        'is_thumbnail'  => 0,
                    ]);
                }
            }

            Session::flash('success', 'Sukses mengubah data Project!');
            return redirect()->route('admin.project.index',['item' => $project->id]);

        }catch(\Exception $ex){
            error_log($ex);
            Log::error("Admin/ProjectController update error: ". $ex);
            return back()->withErrors("Something Went Wrong")->withInput();
        }
    }

    public function getProjects(Request $request){
        $term = trim($request->q);
        $roles = Project::where('id', '!=', $request->id)
            ->where(function ($q) use ($term) {
                $q->where('name', 'LIKE', '%' . $term . '%');
            })
            ->get();

        $formatted_tags = [];

        foreach ($roles as $role) {
            $formatted_tags[] = ['id' => $role->id, 'text' => $role->name];
        }

        return \Response::json($formatted_tags);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        try {
            $deletedId = $request->input('id');
            $project = Project::find($deletedId);
            if(empty($project)){
                return Response::json(array('errors' => 'INVALID'));
            }

            $detailImages = ProjectImage::where('project_id', $deletedId)->get();

            // Delete old image
            foreach ($detailImages as $oldImage){
                if(!empty($oldImage->path)){
                    $oldPath = asset('storage/projects/'. $oldImage->path);
                    if(file_exists($oldPath)) unlink($oldPath);
                }
                $oldImage->delete();
            }

            $project->delete();

            Session::flash('success', 'Sukses menghapus Project!');
            return Response::json(array('success' => 'VALID'));
        }
        catch(\Exception $ex){
            error_log($ex);
            Log::error("Admin/ProjectController destroy error: ". $ex);
            return Response::json(array('errors' => 'INVALID'));
        }


    }
}
