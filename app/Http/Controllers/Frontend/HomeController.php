<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\ContactMessage;
use App\Models\Project;
use App\Models\ProjectImage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('frontend.home');
    }

    public function about()
    {
        return view('frontend.about');
    }

//    public function product()
//    {
//        return view('frontend.product');
//    }

    public function contactUs(){

        return view('frontend.contact-us');
    }

//    public function project()
//    {
//        return view('frontend.project');
//    }

    public function project(){
        $projects = DB::table('projects')->paginate(5);

        $data = [
            'projects'         => $projects,
        ];

        return view('frontend.project')->with($data);
    }

    public function product(){
        $products = DB::table('products')->paginate(5);

        $data = [
            'products'         => $products,
        ];

        return view('frontend.product')->with($data);
    }

    public function saveContactUs(Request $request){
//        dd(Carbon::now('Asia/Jakarta')->toDateTimeString());

        $validator = Validator::make($request->all(), [
            'name'    => 'required',
            'email'     => 'required|regex:/^\S*$/u|email',
//            'subject'          => 'required',
            'message'          => 'required',
        ]);

        if ($validator->fails())
            return redirect()->back()->withErrors($validator->errors())->withInput($request->all());

        $data = ContactMessage::create([
            'name'          => $request->input('name'),
            'email'         => $request->input('email'),
//            'subject'         => $request->input('subject'),
            'message'       => $request->input('message'),
            'created_at'    => Carbon::now('Asia/Jakarta')->toDateTimeString()
        ]);

        Session::flash('success', 'Thank you for Contacting us!');
        return redirect()->route('frontend.contact-us');
    }





}
