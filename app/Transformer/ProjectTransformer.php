<?php
/**
 * Created by PhpStorm.
 * User: YANSEN
 * Date: 12/10/2018
 * Time: 10:03
 */

namespace App\Transformer;


use App\Models\Project;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class ProjectTransformer extends TransformerAbstract
{

    public function transform(Project $project){

        try{
            $createdDate = Carbon::parse($project->created_at)->format('d M Y');
            $updatedDate = Carbon::parse($project->updated_at)->format('d M Y');

            $itemShowUrl = route('admin.project.show', ['item' => $project->id]);
            $itemEditUrl = route('admin.project.edit', ['item' => $project->id]);

            //ambil data project images
            $imageProjs = $project->project_images;
            $imgPath = "";
            foreach($imageProjs as $imageProj){
                if($imageProj->is_main_image == 1){
                    $imageURL = asset('storage/projects/'.$imageProj->path);
                    $imgPath = "<img src='".$imageURL."' width='50' alt='img'>";
                }
            }

//            $action = "<a class='btn btn-xs btn-primary' href='".$itemShowUrl."' data-toggle='tooltip' data-placement='top'><i class='fa fa-info'></i></a> ";
            $action = "<a class='btn btn-xs btn-info' href='".$itemEditUrl."' data-toggle='tooltip' data-placement='top'><i class='fa fa-edit'></i></a> ";
            $action .= "<a class='delete-modal btn btn-xs btn-danger' data-id='". $project->id ."' ><i class='fa fa-remove'></i></a>";


            return[
                'name'              => $project->name,
                'introduction'      => $project->introduction,
                'description'       => $project->description,
                'img_path'          => $imgPath,
                'created_at'        => $createdDate,
                'update_at'         => $updatedDate,
                'action'            => $action
            ];
        }
        catch (\Exception $exception){
            error_log($exception);
        }
    }
}
