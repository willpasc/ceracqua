<?php
/**
 * Created by PhpStorm.
 * User: YANSEN
 * Date: 12/10/2018
 * Time: 10:03
 */

namespace App\Transformer;


use App\Models\Product;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class ProductTransformer extends TransformerAbstract
{

    public function transform(Product $product){

        try{
            $createdDate = Carbon::parse($product->created_at)->format('d M Y');
            $updatedDate = Carbon::parse($product->updated_at)->format('d M Y');

            $itemShowUrl = route('admin.product.show', ['item' => $product->id]);
            $itemEditUrl = route('admin.product.edit', ['item' => $product->id]);

            //ambil data producr images
            $imageProds = $product->product_images;
            $imgPath = "";
            foreach($imageProds as $imageProd){
                if($imageProd->is_main_image == 1){
                    $imageURL = asset('storage/products/'.$imageProd->path);
                    $imgPath = "<img src='".$imageURL."' width='50' alt='img'>";
                }
            }

//            $action = "<a class='btn btn-xs btn-primary' href='".$itemShowUrl."' data-toggle='tooltip' data-placement='top'><i class='fa fa-info'></i></a> ";
            $action = "<a class='btn btn-xs btn-info' href='".$itemEditUrl."' data-toggle='tooltip' data-placement='top'><i class='fa fa-edit'></i></a> ";
            $action .= "<a class='delete-modal btn btn-xs btn-danger' data-id='". $product->id ."' ><i class='fa fa-remove'></i></a>";


            return[
                'name'              => $product->name,
                'category'          => $product->category->name ?? '',
                'introduction'      => $product->introduction,
                'price'             => $product->price,
                'description'       => $product->description,
                'img_path'          => $imgPath,
                'created_at'        => $createdDate,
                'update_at'         => $updatedDate,
                'action'            => $action
            ];
        }
        catch (\Exception $exception){
            error_log($exception);
        }
    }
}
