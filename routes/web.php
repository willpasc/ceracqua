<?php

//use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/clear', function() {

    \Illuminate\Support\Facades\Artisan::call('cache:clear');
    \Illuminate\Support\Facades\Artisan::call('config:clear');
    \Illuminate\Support\Facades\Artisan::call('config:cache');
    \Illuminate\Support\Facades\Artisan::call('view:clear');

    return "Cleared!";

});
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Frontend\HomeController@index')->name('home');
Route::get('/project', 'Frontend\HomeController@project')->name('frontend.project');
Route::get('/product', 'Frontend\HomeController@product')->name('frontend.product');
Route::get('/about', 'Frontend\HomeController@about')->name('frontend.about');
Route::get('/contact-us', 'Frontend\HomeController@contactUs')->name('frontend.contact-us');
Route::post('/contact-us', 'Frontend\HomeController@saveContactUs')->name('frontend.contact_us.save');

Route::get('/project/detail/{id}', 'Frontend\ProjectController@show')->name('frontend.project.show');
Route::get('/product/detail/{id}', 'Frontend\ProductController@show')->name('frontend.product.show');



// ADMIN ROUTE
// ====================================================================================================================

Route::prefix('admin')->group(function(){
    Route::get('/', 'Admin\ProjectController@index')->name('admin.dashboard');
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');

//    // Portofolio
//    Route::get('/portofolio/', 'Admin\PortofolioController@index')->name('admin.portofolio.index');
//    Route::get('/portofolio/show/{item}', 'Admin\PortofolioController@show')->name('admin.portofolio.show');
//    Route::get('/portofolio/create', 'Admin\PortofolioController@create')->name('admin.portofolio.create');
//    Route::post('/portofolio/store', 'Admin\PortofolioController@store')->name('admin.portofolio.store');
//    Route::get('/portofolio/edit/{item}', 'Admin\PortofolioController@edit')->name('admin.portofolio.edit');
//    Route::post('/portofolio/update', 'Admin\PortofolioController@update')->name('admin.portofolio.update');
//    Route::post('/portofolio/delete', 'Admin\PortofolioController@destroy')->name('admin.portofolio.destroy');

    // Project

    Route::get('/project/', 'Admin\ProjectController@index')->name('admin.project.index');
    Route::get('/project/show/{item}', 'Admin\ProjectController@show')->name('admin.project.show');
    Route::get('/project/create', 'Admin\ProjectController@create')->name('admin.project.create');
    Route::post('/project/store', 'Admin\ProjectController@store')->name('admin.project.store');
    Route::get('/project/edit/{item}', 'Admin\ProjectController@edit')->name('admin.project.edit');
    Route::post('/project/update', 'Admin\ProjectController@update')->name('admin.project.update');
    Route::post('/project/delete', 'Admin\ProjectController@destroy')->name('admin.project.destroy');

    // Project

    Route::get('/product/', 'Admin\ProductController@index')->name('admin.product.index');
    Route::get('/product/show/{item}', 'Admin\ProductController@show')->name('admin.product.show');
    Route::get('/product/create', 'Admin\ProductController@create')->name('admin.product.create');
    Route::post('/product/store', 'Admin\ProductController@store')->name('admin.product.store');
    Route::get('/product/edit/{item}', 'Admin\ProductController@edit')->name('admin.product.edit');
    Route::post('/product/update', 'Admin\ProductController@update')->name('admin.product.update');
    Route::post('/product/delete', 'Admin\ProductController@destroy')->name('admin.product.destroy');

    // Contact Message
    Route::get('/contact-messages', 'Admin\ContactMessageController@index')->name('admin.contact-messages.index');

    // Admin User
    Route::get('/admin-users', 'Admin\AdminUserController@index')->name('admin.admin-users.index');
    Route::get('/admin-users/create', 'Admin\AdminUserController@create')->name('admin.admin-users.create');
    Route::post('/admin-users/store', 'Admin\AdminUserController@store')->name('admin.admin-users.store');
    Route::get('/admin-users/edit/{item}', 'Admin\AdminUserController@edit')->name('admin.admin-users.edit');
    Route::post('/admin-users/update', 'Admin\AdminUserController@update')->name('admin.admin-users.update');
    Route::post('/admin-users/delete', 'Admin\AdminUserController@destroy')->name('admin.admin-users.destroy');

    // User
    Route::get('/users', 'Admin\UserController@index')->name('admin.users.index');
    Route::get('/users/create', 'Admin\UserController@create')->name('admin.users.create');
    Route::post('/users/store', 'Admin\UserController@store')->name('admin.users.store');
    Route::get('/users/edit/{item}', 'Admin\UserController@edit')->name('admin.users.edit');
    Route::post('/users/update', 'Admin\UserController@update')->name('admin.users.update');
    Route::post('/users/delete', 'Admin\UserController@destroy')->name('admin.users.destroy');

    // Subscribes
    Route::get('/subscribes', 'Admin\SubscribeController@index')->name('admin.subscribes.index');
    Route::get('/subscribe-downloads', 'Admin\SubscribeController@download')->name('admin.subscribes.download');

    Route::get('/admin/change-password', 'Admin\AdminController@editPassword')->name('admin.change-password');
    Route::post('/admin/change-password/save', 'Admin\AdminController@updatePassword')->name('admin.change-password.save');
});


//Route::get('/datatable/portofolio', 'Admin\PortofolioController@getIndex')->name('datatables.portofolio');
Route::get('/datatable/project', 'Admin\ProjectController@getIndex')->name('datatables.project');
Route::get('/datatable/product', 'Admin\ProductController@getIndex')->name('datatables.product');
Route::get('/datatable/contact-message', 'Admin\ContactMessageController@getIndex')->name('datatables.contact-message');
