@extends('layouts.frontend')

@section('head_and_title')
    <meta name="description" content="Ceracqua Contact us">
    <meta name="author" content="PT. Generasi Muda Gigih">
    <meta name="keywords"
          content="Ceracqua Imagi Kreasindo, Ceramic, Ceramic Decoration, Ceramic Digital Printing, Creative Industry">

    <title>PT Ceracqua Imagi Kreasindo</title>
@endsection

@section('content')

	<main class="bg_gray">

{{--			<div class="container margin_60">--}}
{{--				<div class="main_title">--}}
{{--					<h2>Contact Us</h2>--}}
{{--					<p></p>--}}
{{--				</div>--}}
{{--				<div class="row justify-content-center">--}}
{{--					<div class="col-lg-4">--}}
{{--						<div class="box_contacts">--}}
{{--							<i class="ti-support"></i>--}}
{{--							<h2>PT Ceracqua Imagi Kreasindo</h2>--}}
{{--							<a href="#0">+94 423-23-221</a> - <a href="#0">help@allaia.com</a>--}}
{{--							<small>MON to FRI 9am-6pm SAT 9am-2pm</small>--}}
{{--						</div>--}}
{{--					</div>--}}
{{--					<div class="col-lg-4">--}}
{{--						<div class="box_contacts font-weight-bold">--}}
{{--							<i class="ti-map-alt"></i>--}}
{{--							<h2>PT Ceracqua Imagi Kreasindo</h2>--}}
{{--							<div>--}}
{{--                                <i class="ti-home"></i>--}}
{{--                                <p>Ruko Sutera Niaga II no. 56<br/>--}}
{{--                                Jalan Raya Serpong,<br/>--}}
{{--                                Alam Sutera – Tangerang</p>--}}
{{--                            </div>--}}
{{--                            <div>--}}
{{--                                <i class="ti-headphone-alt"></i>--}}
{{--                                <p>(021) - 5312 1596</p>--}}
{{--                            </div>--}}
{{--                            <div>--}}
{{--                                <i class="ti-email"></i>--}}
{{--                                <p>ceracqua.imagio@gmail.com</p>--}}
{{--                            </div>--}}

{{--						</div>--}}
{{--					</div>--}}
{{--					<div class="col-lg-4">--}}
{{--						<div class="box_contacts">--}}
{{--							<i class="ti-package"></i>--}}
{{--							<h2>Allaia Orders</h2>--}}
{{--							<a href="#0">+94 423-23-221</a> - <a href="#0">order@allaia.com</a>--}}
{{--							<small>MON to FRI 9am-6pm SAT 9am-2pm</small>--}}
{{--						</div>--}}
{{--					</div>--}}
{{--				</div>--}}
{{--				<!-- /row -->--}}
{{--			</div>--}}
			<!-- /container -->
		<div class="bg_white">
			<div class="container margin_60_35">
				<h4 class="pb-3">Drop Us a Line</h4>

                {{ Form::open(['route'=>['frontend.contact_us.save'],'method' => 'post','id' => 'contact-form', ]) }}

                @if(\Illuminate\Support\Facades\Session::has('success'))
                    <div class="row mb-2">
                        <div class=" col-md-12">
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <strong>{{ \Illuminate\Support\Facades\Session::get('success') }}</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </div>
                @endif

                @foreach($errors->all() as $error)
                    <ul>
                        <li>
                                <span class="help-block">
                                    <strong style="color: #ff3d00;"> {{ $error }} </strong>
                                </span>
                        </li>
                    </ul>
                @endforeach


				<div class="row">
					<div class="col-lg-4 col-md-6 add_bottom_25">
						<div class="form-group">
							<input class="form-control" type="text" name="name" placeholder="Name *" value="{{old('name')}}">
						</div>
						<div class="form-group">
							<input class="form-control" type="email" name="email" placeholder="Email *" value="{{old('email')}}">
						</div>
						<div class="form-group">
							<textarea class="form-control" style="height: 150px;" name="message" placeholder="Message *">{{old('message')}}</textarea>
						</div>
						<div class="form-group">
							<input class="btn_1 full-width" type="submit" value="SUBMIT">
						</div>
					</div>
					<div class="col-lg-8 col-md-6 add_bottom_25">
{{--					<iframe class="map_contact" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d39714.47749917409!2d-0.13662037019554393!3d51.52871971170425!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47d8a00baf21de75%3A0x52963a5addd52a99!2sLondra%2C+Regno+Unito!5e0!3m2!1sit!2ses!4v1557824540343!5m2!1sit!2ses" style="border: 0" allowfullscreen></iframe>--}}
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.126747182023!2d106.64785601529528!3d-6.247023762908828!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69fb95b0a591cb%3A0x8b6adcf08eef3881!2sPT.CERACQUA%20IMAGI%20KREASINDO!5e0!3m2!1sen!2sid!4v1593250789959!5m2!1sen!2sid" width="800" height="350" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                    </div>
				</div>
            {{ Form::close() }}
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /bg_white -->
	</main>
	<!--/main-->
@endsection

	<!-- page -->

	<div id="toTop"></div><!-- Back to top button -->

