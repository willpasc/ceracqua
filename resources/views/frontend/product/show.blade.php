@extends('layouts.frontend')

@section('head_and_title')
    <meta name="description" content="Ceracqua Project Detail">
    <meta name="author" content="PT. Generasi Muda Gigih">
    <meta name="keywords"
          content="Ceracqua Imagi Kreasindo, Ceramic, Ceramic Decoration, Ceramic Digital Printing, Creative Industry">


    <title>PT Ceracqua Imagi Kreasindo</title>
@endsection



@section('content')

    <main class="tab_content_wrapper">
        <div class="container margin_30 ">
            {{--	        <div class="countdown_inner">-20% This offer ends in <div data-countdown="2019/05/15" class="countdown"></div>--}}
            <div class="row text-center">
                <div class="col-12">
                    <h1 class="pb-5"> {{ $product->name }}</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-12 text-center">
                    <div class="all">
                        <div class="slider">
                            <div class="owl-carousel owl-theme main">
                                @php( $images = \App\Models\ProductImage::where('product_id',
                                        $product->id)->orderBy('is_main_image')->get() )
                                @php( $main_image = \App\Models\ProductImage::where('is_main_image', 1)->where('product_id', $product->id) ->first() )


                                <div style="background-image: url('{{ asset('storage/products/'. $main_image->path) }}');" class="item-box active" ></div>

                                    @foreach($images as $image)
                                    <div style="background-image: url('{{ asset('storage/products/'. $image->path) }}');" class="item-box"></div>
                                @endforeach
{{--                                <div style="background-image: url('{{ asset('image/ceracqua/portofolios/blok m1.jpg') }}');" class="item-box"></div>--}}
{{--                                <div style="background-image: url('{{ asset('image/ceracqua/portofolios/blok m2.jpg') }}');" class="item-box"></div>--}}
{{--                                <div style="background-image: url('{{ asset('image/ceracqua/portofolios/blok m3.jpg') }}');" class="item-box"></div>--}}
{{--                                <div style="background-image: url('{{ asset('image/ceracqua/portofolios/blok m4.jpg') }}');" class="item-box"></div>--}}
{{--                                <div style="background-image: url('{{ asset('image/ceracqua/portofolios/blok m5.jpg') }}');" class="item-box"></div>--}}

                            </div>
                            <div class="left nonl"><i class="ti-angle-left"></i></div>
                            <div class="right"><i class="ti-angle-right"></i></div>
                        </div>
                        <div class="slider-two">
                            <div class="owl-carousel owl-theme thumbs">
                                @foreach($images as $image)
                                    <div style="background-image: url('{{ asset('storage/products/'. $image->path) }}');" class="item"></div>
                                @endforeach
{{--                                <div style="background-image: url('{{ asset('image/ceracqua/portofolios/blok m1.jpg') }}');" class="item active"></div>--}}
{{--                                <div style="background-image: url('{{ asset('image/ceracqua/portofolios/blok m2.jpg') }}');" class="item "></div>--}}
{{--                                <div style="background-image: url('{{ asset('image/ceracqua/portofolios/blok m3.jpg') }}');" class="item"></div>--}}
{{--                                <div style="background-image: url('{{ asset('image/ceracqua/portofolios/blok m4.jpg') }}');" class="item "></div>--}}
{{--                                <div style="background-image: url('{{ asset('image/ceracqua/portofolios/blok m5.jpg') }}');" class="item "></div>--}}

                            </div>
                            <div class="left-t nonl-t"></div>
                            <div class="right-t"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="card-body">
                        <div class="row justify-content-between">
                            <div class="col-12">
                                <h3>Price</h3>
                                <p>Rp. {{ number_format($product->price, 0, ",", ".") }} </p>
                            </div>
                            <div class="col-12">
                                <h3>Details</h3>
                                <p>{!!nl2br($product->description)!!} </p>
                            </div>
{{--                            <div class="col-12">--}}
{{--                                <h3>Specifications</h3>--}}
{{--                                <div class="table-responsive">--}}
{{--                                    <table class="table table-sm table-striped">--}}
{{--                                        <tbody>--}}
{{--                                        <tr>--}}
{{--                                            <td><strong>Color</strong></td>--}}
{{--                                            <td>Blue, Purple</td>--}}
{{--                                        </tr>--}}
{{--                                        <tr>--}}
{{--                                            <td><strong>Size</strong></td>--}}
{{--                                            <td>150x100x100</td>--}}
{{--                                        </tr>--}}
{{--                                        <tr>--}}
{{--                                            <td><strong>Weight</strong></td>--}}
{{--                                            <td>0.6kg</td>--}}
{{--                                        </tr>--}}
{{--                                        <tr>--}}
{{--                                            <td><strong>Manifacturer</strong></td>--}}
{{--                                            <td>Manifacturer</td>--}}
{{--                                        </tr>--}}
{{--                                        </tbody>--}}
{{--                                    </table>--}}
{{--                                </div>--}}
{{--                                <!-- /table-responsive -->--}}
{{--                            </div>--}}
                        </div>
                    </div>
                </div>
            <!-- /row -->
            </div>
            <!-- /container -->

{{--            <div class="tabs_product">--}}
{{--                <div class="container">--}}
{{--                    <ul class="nav nav-tabs" role="tablist">--}}
{{--                        <li class="nav-item">--}}
{{--                            <a id="tab-A" href="#pane-A" class="nav-link active" data-toggle="tab" role="tab">Description</a>--}}
{{--                        </li>--}}
{{--                        --}}{{--	                <li class="nav-item">--}}
{{--                        --}}{{--	                    <a id="tab-B" href="#pane-B" class="nav-link" data-toggle="tab" role="tab">Reviews</a>--}}
{{--                        --}}{{--	                </li>--}}
{{--                    </ul>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <!-- /tabs_product -->--}}
{{--            <div class="tab_content_wrapper">--}}
{{--                <div class="container">--}}
{{--                    <div class="tab-content" role="tablist">--}}
{{--                        <div id="pane-A" class="card tab-pane fade active show" role="tabpanel" aria-labelledby="tab-A">--}}
{{--                            <div class="card-header" role="tab" id="heading-A">--}}
{{--                                <h5 class="mb-0">--}}
{{--                                    <a class="collapsed" data-toggle="collapse" href="#collapse-A" aria-expanded="false" aria-controls="collapse-A">--}}
{{--                                        Description--}}
{{--                                    </a>--}}
{{--                                </h5>--}}
{{--                            </div>--}}
{{--                            <div id="collapse-A" class="collapse" role="tabpanel" aria-labelledby="heading-A">--}}
{{--                                <div class="card-body">--}}
{{--                                    <div class="row justify-content-between">--}}
{{--                                        <div class="col-lg-6">--}}
{{--                                            <h3>Details</h3>--}}
{{--                                            <p>{!!nl2br($product->description)!!} </p>--}}
{{--                                        </div>--}}
{{--                                        <div class="col-lg-5">--}}
{{--                                            <h3>Specifications</h3>--}}
{{--                                            <div class="table-responsive">--}}
{{--                                                <table class="table table-sm table-striped">--}}
{{--                                                    <tbody>--}}
{{--                                                    <tr>--}}
{{--                                                        <td><strong>Color</strong></td>--}}
{{--                                                        <td>Blue, Purple</td>--}}
{{--                                                    </tr>--}}
{{--                                                    <tr>--}}
{{--                                                        <td><strong>Size</strong></td>--}}
{{--                                                        <td>150x100x100</td>--}}
{{--                                                    </tr>--}}
{{--                                                    <tr>--}}
{{--                                                        <td><strong>Weight</strong></td>--}}
{{--                                                        <td>0.6kg</td>--}}
{{--                                                    </tr>--}}
{{--                                                    <tr>--}}
{{--                                                        <td><strong>Manifacturer</strong></td>--}}
{{--                                                        <td>Manifacturer</td>--}}
{{--                                                    </tr>--}}
{{--                                                    </tbody>--}}
{{--                                                </table>--}}
{{--                                            </div>--}}
{{--                                            <!-- /table-responsive -->--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <!-- /TAB A -->--}}
{{--                        <div id="pane-B" class="card tab-pane fade" role="tabpanel" aria-labelledby="tab-B">--}}
{{--                            <div class="card-header" role="tab" id="heading-B">--}}
{{--                                <h5 class="mb-0">--}}
{{--                                    <a class="collapsed" data-toggle="collapse" href="#collapse-B" aria-expanded="false" aria-controls="collapse-B">--}}
{{--                                        Reviews--}}
{{--                                    </a>--}}
{{--                                </h5>--}}
{{--                            </div>--}}
{{--                            <div id="collapse-B" class="collapse" role="tabpanel" aria-labelledby="heading-B">--}}
{{--                                <div class="card-body">--}}
{{--                                    <div class="row justify-content-between">--}}
{{--                                        <div class="col-lg-6">--}}
{{--                                            <div class="review_content">--}}
{{--                                                <div class="clearfix add_bottom_10">--}}
{{--                                                    <span class="rating"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><em>5.0/5.0</em></span>--}}
{{--                                                    <em>Published 54 minutes ago</em>--}}
{{--                                                </div>--}}
{{--                                                <h4>"Commpletely satisfied"</h4>--}}
{{--                                                <p>Eos tollit ancillae ea, lorem consulatu qui ne, eu eros eirmod scaevola sea. Et nec tantas accusamus salutatus, sit commodo veritus te, erat legere fabulas has ut. Rebum laudem cum ea, ius essent fuisset ut. Viderer petentium cu his.</p>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="col-lg-6">--}}
{{--                                            <div class="review_content">--}}
{{--                                                <div class="clearfix add_bottom_10">--}}
{{--                                                    <span class="rating"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star empty"></i><i class="icon-star empty"></i><em>4.0/5.0</em></span>--}}
{{--                                                    <em>Published 1 day ago</em>--}}
{{--                                                </div>--}}
{{--                                                <h4>"Always the best"</h4>--}}
{{--                                                <p>Et nec tantas accusamus salutatus, sit commodo veritus te, erat legere fabulas has ut. Rebum laudem cum ea, ius essent fuisset ut. Viderer petentium cu his.</p>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <!-- /row -->--}}
{{--                                    <div class="row justify-content-between">--}}
{{--                                        <div class="col-lg-6">--}}
{{--                                            <div class="review_content">--}}
{{--                                                <div class="clearfix add_bottom_10">--}}
{{--                                                    <span class="rating"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star empty"></i><em>4.5/5.0</em></span>--}}
{{--                                                    <em>Published 3 days ago</em>--}}
{{--                                                </div>--}}
{{--                                                <h4>"Outstanding"</h4>--}}
{{--                                                <p>Eos tollit ancillae ea, lorem consulatu qui ne, eu eros eirmod scaevola sea. Et nec tantas accusamus salutatus, sit commodo veritus te, erat legere fabulas has ut. Rebum laudem cum ea, ius essent fuisset ut. Viderer petentium cu his.</p>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="col-lg-6">--}}
{{--                                            <div class="review_content">--}}
{{--                                                <div class="clearfix add_bottom_10">--}}
{{--                                                    <span class="rating"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><em>5.0/5.0</em></span>--}}
{{--                                                    <em>Published 4 days ago</em>--}}
{{--                                                </div>--}}
{{--                                                <h4>"Excellent"</h4>--}}
{{--                                                <p>Sit commodo veritus te, erat legere fabulas has ut. Rebum laudem cum ea, ius essent fuisset ut. Viderer petentium cu his.</p>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <!-- /row -->--}}
{{--                                    <p class="text-right"><a href="leave-review.html" class="btn_1">Leave a review</a></p>--}}
{{--                                </div>--}}
{{--                                <!-- /card-body -->--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <!-- /tab B -->--}}
{{--                    </div>--}}
{{--                    <!-- /tab-content -->--}}
{{--                </div>--}}
{{--                <!-- /container -->--}}
{{--            </div>--}}
            <!-- /tab_content_wrapper -->

        <!--/feat-->
        </div>
    </main>
    <!-- /main -->
@endsection

<!-- page -->

<div id="toTop"></div><!-- Back to top button -->

<div class="top_panel">
    <div class="container header_panel">
        <a href="#0" class="btn_close_top_panel"><i class="ti-close"></i></a>
        <label>1 product added to cart</label>
    </div>
    <!-- /header_panel -->
    <div class="item">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <div class="item_panel">
                        <figure>
                            <img src="img/products/product_placeholder_square_small.jpg" data-src="img/products/shoes/1.jpg" class="lazy" alt="">
                        </figure>
                        <h4>1x Armor Air X Fear</h4>
                        <div class="price_panel"><span class="new_price">$148.00</span><span class="percentage">-20%</span> <span class="old_price">$160.00</span></div>
                    </div>
                </div>
                <div class="col-md-5 btn_panel">
                    <a href="cart.html" class="btn_1 outline">View cart</a> <a href="checkout.html" class="btn_1">Checkout</a>
                </div>
            </div>
        </div>
    </div>
    <!-- /item -->
    <div class="container related">
        <h4>Who bought this product also bought</h4>
        <div class="row">
            <div class="col-md-4">
                <div class="item_panel">
                    <a href="#0">
                        <figure>
                            <img src="img/products/product_placeholder_square_small.jpg" data-src="img/products/shoes/2.jpg" alt="" class="lazy">
                        </figure>
                    </a>
                    <a href="#0">
                        <h5>Armor Okwahn II</h5>
                    </a>
                    <div class="price_panel"><span class="new_price">$90.00</span></div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="item_panel">
                    <a href="#0">
                        <figure>
                            <img src="img/products/product_placeholder_square_small.jpg" data-src="img/products/shoes/3.jpg" alt="" class="lazy">
                        </figure>
                    </a>
                    <a href="#0">
                        <h5>Armor Air Wildwood ACG</h5>
                    </a>
                    <div class="price_panel"><span class="new_price">$75.00</span><span class="percentage">-20%</span> <span class="old_price">$155.00</span></div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="item_panel">
                    <a href="#0">
                        <figure>
                            <img src="img/products/product_placeholder_square_small.jpg" data-src="img/products/shoes/4.jpg" alt="" class="lazy">
                        </figure>
                    </a>
                    <a href="#0">
                        <h5>Armor ACG React Terra</h5>
                    </a>
                    <div class="price_panel"><span class="new_price">$110.00</span></div>
                </div>
            </div>
        </div>
    </div>
    <!-- /related -->
</div>
<!-- /add_cart_panel -->

<!-- Size modal -->
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="size-modal" id="size-modal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Size guide</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="ti-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <p>Lorem ipsum dolor sit amet, et velit propriae invenire mea, ad nam alia intellegat. Aperiam mediocrem rationibus nec te. Tation persecuti accommodare pro te. Vis et augue legere, vel labitur habemus ocurreret ex.</p>
                <div class="table-responsive">
                    <table class="table table-striped table-sm sizes">
                        <tbody><tr>
                            <th scope="row">US Sizes</th>
                            <td>6</td>
                            <td>6,5</td>
                            <td>7</td>
                            <td>7,5</td>
                            <td>8</td>
                            <td>8,5</td>
                            <td>9</td>
                            <td>9,5</td>
                            <td>10</td>
                            <td>10,5</td>
                        </tr>
                        <tr>
                            <th scope="row">Euro Sizes</th>
                            <td>39</td>
                            <td>39</td>
                            <td>40</td>
                            <td>40-41</td>
                            <td>41</td>
                            <td>41-42</td>
                            <td>42</td>
                            <td>42-43</td>
                            <td>43</td>
                            <td>43-44</td>
                        </tr>
                        <tr>
                            <th scope="row">UK Sizes</th>
                            <td>5,5</td>
                            <td>6</td>
                            <td>6,5</td>
                            <td>7</td>
                            <td>7,5</td>
                            <td>8</td>
                            <td>8,5</td>
                            <td>9</td>
                            <td>9,5</td>
                            <td>10</td>
                        </tr>
                        <tr>
                            <th scope="row">Inches</th>
                            <td>9.25"</td>
                            <td>9.5"</td>
                            <td>9.625"</td>
                            <td>9.75"</td>
                            <td>9.9375"</td>
                            <td>10.125"</td>
                            <td>10.25"</td>
                            <td>10.5"</td>
                            <td>10.625"</td>
                            <td>10.75"</td>
                        </tr>
                        <tr>
                            <th scope="row">CM</th>
                            <td>23,5</td>
                            <td>24,1</td>
                            <td>24,4</td>
                            <td>24,8</td>
                            <td>25,4</td>
                            <td>25,7</td>
                            <td>26</td>
                            <td>26,7</td>
                            <td>27</td>
                            <td>27,3</td>
                        </tr>
                        </tbody></table>
                </div>
                <!-- /table -->
            </div>
        </div>
    </div>
</div>


