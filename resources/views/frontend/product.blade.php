@extends('layouts.frontend')

@section('head_and_title')
    <meta name="description" content="Ceracacqua Product">
    <meta name="author" content="PT. Generasi Muda Gigih">
    <meta name="keywords"
          content="Ceracqua Imagi Kreasindo, Ceramic, Ceramic Decoration, Ceramic Digital Printing, Creative Industry">

    <title>PT Ceracqua Imagi Kreasindo</title>
@endsection


@section('content')
	<main>
	    <div class="top_banner">
	        <div class="opacity-mask d-flex align-items-center" data-opacity-mask="rgba(0, 0, 0, 0.3)">
	            <div class="container">
{{--	                <div class="breadcrumbs">--}}
{{--	                    <ul>--}}
{{--	                        <li><a href="#">Home</a></li>--}}
{{--	                        <li><a href="#">Category</a></li>--}}
{{--	                        <li>Page active</li>--}}
{{--	                    </ul>--}}
{{--	                </div>--}}
	                <h1>PRODUCTS</h1>
	            </div>
	        </div>
	        <img src="{{asset('img/bg_cat_shoes.jpg')}}" class="img-fluid" alt="">
	    </div>
	    <!-- /top_banner -->
	    <div id="stick_here"></div>
{{--	    <div class="toolbox elemento_stick">--}}
{{--	        <div class="container">--}}
{{--	            <ul class="clearfix">--}}
{{--	                <li>--}}
{{--	                    <div class="sort_select">--}}
{{--	                        <select name="sort" id="sort">--}}
{{--	                            <option value="popularity" selected="selected">Sort by popularity</option>--}}
{{--	                            <option value="rating">Sort by average rating</option>--}}
{{--	                            <option value="date">Sort by newness</option>--}}
{{--	                            <option value="price">Sort by price: low to high</option>--}}
{{--	                            <option value="price-desc">Sort by price: high to--}}
{{--	                        </select>--}}
{{--	                    </div>--}}
{{--	                </li>--}}
{{--	                <li>--}}
{{--	                    <a href="#0"><i class="ti-view-grid"></i></a>--}}
{{--	                    <a href="listing-row-1-sidebar-left.html"><i class="ti-view-list"></i></a>--}}
{{--	                </li>--}}
{{--	                <li>--}}
{{--	                    <a href="#0" class="open_filters">--}}
{{--	                        <i class="ti-filter"></i><span>Filters</span>--}}
{{--	                    </a>--}}
{{--	                </li>--}}
{{--	            </ul>--}}
{{--	        </div>--}}
{{--	    </div>--}}
	    <!-- /toolbox -->
	    <div class="container margin_30">
	        <div class="row">
	            <aside class="col-lg-3" id="sidebar_fixed">
	                <div class="filter_col">
	                    <div class="inner_bt"><a href="#" class="open_filters"><i class="ti-close"></i></a></div>
                        <div class="row no-gutters">
                            <div class="col-md-12 form-group">
                                <div class="custom-select-form">
                                    <select class="wide " name="category" id="category" style="display: none;">
                                        <option value="" selected="">- Category -</option>
                                        <option value="walltile">Ceracqua Wall Tile</option>
                                        <option value="floortile">Ceracqua Floor Tile</option>
                                        <option value="mosaic">Ceracqua Mosaic</option>
                                        <option value="decoration">Ceracqua Decoration</option>
                                        <option value="project">Ceracqua Project</option>
                                    </select>
                                    <div class="nice-select wide " tabindex="0">
                                        <span class="current">- Category -</span>
                                        <ul class="list">
                                            <li data-value="walltile" class="option">Ceracqua Wall Tile</li>
                                            <li data-value="floortile" class="option">Ceracqua Floor Tile</li>
                                            <li data-value="mosaic" class="option">Ceracqua Mosaic</li>
                                            <li data-value="decoration" class="option">Ceracqua Decoration</li>
                                            <li data-value="project" class="option">Ceracqua Project</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row no-gutters">
                            <div class="col-md-12 form-group">
                                <div class="custom-select-form">
                                    <select class="wide" name="color" id="color" style="display: none;">
                                        <option value="" selected="">- Color -</option>
                                        <option value="red">Red</option>
                                        <option value="black">Black</option>
                                        <option value="blue">Blue</option>
                                        <option value="yellow">Yellow</option>
                                        <option value="pink">Pink</option>
                                    </select>
                                    <div class="nice-select wide" tabindex="0">
                                        <span class="current">- Color -</span>
                                        <ul class="list">
                                            <li data-value="red" class="option">Red</li>
                                            <li data-value="black" class="option">Black</li>
                                            <li data-value="blue" class="option">Blue</li>
                                            <li data-value="yellow" class="option">Yellow</li>
                                            <li data-value="pink" class="option">Pink</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row no-gutters">
                            <div class="col-md-12 form-group">
                                <div class="custom-select-form">
                                    <select class="wide" name="texture" id="texture" style="display: none;">
                                        <option value="" selected="">- Texture -</option>
                                        <option value="anti-slip">Anti Slip</option>
                                        <option value="emboss">Emboss</option>
                                        <option value="glossy">Glossy</option>
                                        <option value="kasar">Kasar</option>
                                        <option value="matte">Matte</option>
                                        <option value="satin">Satin</option>
                                        <option value="no-texture">No Texture</option>
                                    </select>
                                    <div class="nice-select wide" tabindex="0">
                                        <span class="current">- Texture -</span>
                                        <ul class="list">
                                            <li data-value="anti-slip" class="option">Anti Slip</li>
                                            <li data-value="emboss" class="option">Emboss</li>
                                            <li data-value="glossy" class="option">Glossy</li>
                                            <li data-value="kasar" class="option">Kasar</li>
                                            <li data-value="matte" class="option">Matte</li>
                                            <li data-value="satin" class="option">Satin</li>
                                            <li data-value="no-texture" class="option">No Texture</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row no-gutters">
                            <div class="col-md-12 form-group">
                                <div class="custom-select-form">
                                    <select class="wide" name="motive" id="motive" style="display: none;">
                                        <option value="" selected="">- Motive -</option>
                                        <option value="batu">Batu</option>
                                        <option value="grafis">Grafis</option>
                                        <option value="kayu">Kayu</option>
                                        <option value="marmer">Marmer</option>
                                        <option value="polos">Polos</option>
                                    </select>
                                    <div class="nice-select wide" tabindex="0">
                                        <span class="current">- Motive -</span>
                                        <ul class="list">
                                            <li data-value="batu" class="option">Batu</li>
                                            <li data-value="grafis" class="option">Grafis</li>
                                            <li data-value="kayu" class="option">Kayu</li>
                                            <li data-value="marmer" class="option">Marmer</li>
                                            <li data-value="polos" class="option">Polos</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
	                    <!-- /filter_type -->
	                    <!-- /filter_type -->
	                    <div class="buttons">
	                        <a href="#" class="btn_1">Filter</a> <a href="#" class="btn_1 gray">Reset</a>
	                    </div>
	                </div>
	            </aside>
	            <!-- /col -->
	            <div class="col-lg-9">
	                <!-- /row_item -->
                    @foreach($products as $product)
                        <div class="row row_item pb-3">

                            <div class="col-sm-4">
                                <figure>
                                    <a href="{{ route('frontend.product.show', ['id' => $product->id]) }}">
                                        {{--	                            <span class="ribbon off">-30%</span>--}}
                                        @php( $mainImage = \App\Models\ProductImage::where('product_id',
                                        $product->id)->where('is_main_image', 1)->first() )
                                        @if(!empty($mainImage->path))
                                            <img src="{{ asset('storage/products/'. $mainImage->path) }}" class="img-fluid lazy" alt="image project">
                                        @else
                                            <img src="" class="img-fluid lazy" alt="image project">
                                        @endif

                                        {{--	                            <div data-countdown="2019/05/15" class="countdown"></div>--}}
                                    </a>
                                </figure>
                            </div>
                            <div class="col-sm-8 pt-3">
                                {{--	                        <div class="rating"><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star"></i></div>--}}
                                <a href="{{ route('frontend.product.show', ['id' => $product->id]) }}">
                                    <h1>{{ $product->name }}</h1>
                                </a>
                                {{--                            <div style="display: inline-block;">--}}
                                {{--                                {{ $projects->links() }}--}}
                                {{--                            </div>--}}
                                <p>{{ $product->introduction }}</p>
                                <p>Rp. {{ number_format($product->price, 0, ",", ".") }}</p>
                                {{--	                        <div class="price_box">--}}
                                {{--	                            <span class="new_price">$48.00</span>--}}
                                {{--	                            <span class="old_price">$60.00</span>--}}
                                {{--	                        </div>--}}
                                {{--	                        <ul>--}}
                                {{--	                            <li><a href="#0" class="btn_1">Add to cart</a></li>--}}
                                {{--	                            <li><a href="#0" class="btn_1 gray tooltip-1" data-toggle="tooltip" data-placement="top" title="Add to favorites"><i class="ti-heart"></i><span>Add to favorites</span></a></li>--}}
                                {{--	                            <li><a href="#0" class="btn_1 gray tooltip-1" data-toggle="tooltip" data-placement="top" title="Add to compare"><i class="ti-control-shuffle"></i><span>Add to compare</span></a></li>--}}
                                {{--	                        </ul>--}}
                            </div>
                        </div>
                    @endforeach

{{--	                <div class="row row_item">--}}
{{--	                    <div class="col-sm-4">--}}
{{--	                        <figure>--}}
{{--                                <a href="{{route('frontend.product.decoration.product-detail-1')}}">--}}
{{--	                                <img class="img-fluid lazy" src="img/products/product_placeholder_square_medium.jpg" data-src="{{ asset('image/ceracqua/products/decoration/kanvas 20x20cm_1.jpg') }}" alt="">--}}
{{--	                            </a>--}}
{{--	                        </figure>--}}
{{--	                    </div>--}}
{{--	                    <div class="col-sm-8">--}}
{{--                            <a href="{{route('frontend.product.decoration.product-detail-1')}}">--}}
{{--	                            <h3>Kanvas 20x20cm</h3>--}}
{{--	                        </a>--}}
{{--	                        <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident...</p>--}}
{{--	                        <div class="price_box">--}}
{{--	                            <span class="new_price">Rp.50.000</span>--}}
{{--	                            <span class="old_price">$155.00</span>--}}
{{--	                        </div>--}}
{{--	                    </div>--}}
{{--	                </div>--}}
{{--	                <!-- /row_item -->--}}
{{--	                <div class="row row_item">--}}
{{--	                    <div class="col-sm-4">--}}
{{--	                        <figure>--}}
{{--                                <a href="{{route('frontend.product.decoration.product-detail-2')}}">--}}
{{--	                                <img class="img-fluid lazy" src="img/products/product_placeholder_square_medium.jpg" data-src="{{ asset('image/ceracqua/products/decoration/kanvas 20x30cm_1.jpg') }}" alt="">--}}
{{--	                            </a>--}}
{{--	                        </figure>--}}
{{--	                    </div>--}}
{{--	                    <div class="col-sm-8">--}}
{{--                            <a href="{{route('frontend.product.decoration.product-detail-2')}}">--}}
{{--	                            <h3>Kanvas 20x30cm</h3>--}}
{{--	                        </a>--}}
{{--	                        <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident...</p>--}}
{{--	                        <div class="price_box">--}}
{{--	                            <span class="new_price">Rp. 60.000</span>--}}
{{--	                        </div>--}}
{{--	                    </div>--}}
{{--	                </div>--}}
	                <!-- /row_item -->

                    <div class="pagination__wrapper">
                        <ul class="pagination">
                            {{ $products->links() }}
                            {{--                        <li><a href="#0" class="prev" title="previous page">&#10094;</a></li>--}}
                            {{--                        <li>--}}
                            {{--                            <a href="#0" class="active">1</a>--}}
                            {{--                        </li>--}}
                            {{--                        <li>--}}
                            {{--                            <a href="#0">2</a>--}}
                            {{--                        </li>--}}
                            {{--                        <li>--}}
                            {{--                            <a href="#0">3</a>--}}
                            {{--                        </li>--}}
                            {{--                        <li>--}}
                            {{--                            <a href="#0">4</a>--}}
                            {{--                        </li>--}}
                            {{--                        <li><a href="#0" class="next" title="next page">&#10095;</a></li>--}}
                        </ul>
                    </div>
	            </div>
	            <!-- /col -->
	        </div>
	        <!-- /row -->
	    </div>
	    <!-- /container -->
	</main>
	<!-- /main -->

	<!--/footer-->
	</div>
@endsection
	<!-- page -->

	<div id="toTop"></div><!-- Back to top button -->

