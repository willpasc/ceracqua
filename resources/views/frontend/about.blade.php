@extends('layouts.frontend')

@section('head_and_title')
<meta name="description" content="Ceracqua About Us">
<meta name="author" content="PT. Generasi Muda Gigih">
<meta name="keywords"
      content="Ceracqua Imagi Kreasindo, Ceramic, Ceramic Decoration, Ceramic Digital Printing, Creative Industry">

<title>PT Ceracqua Imagi Kreasindo</title>
@endsection

@section('content')



	<main class="bg_gray">
			<div class="container margin_60_35 add_bottom_30">
				<div class="main_title px-4 px-md-0">
					<h2 class="pb-3">PT. CERACQUA IMAGI KREASINDO</h2>
                    <p>PT. Ceracqua Imagi Kreasindo ( Imagio ) berdiri tahun 2013.</p>
                    <p>Visi kami adalah untuk berkontribusi di bidang industri kreatif di Indonesia.</p>
				</div>
				<div class="row justify-content-center align-items-center">
					<div class="col-lg-5">
						<div class="box_about">
							<h2>IMAGIO</h2>
                            <div class="d-block d-md-none">
                                <img src="{{ asset('image/ceracqua/about-1.png') }}" alt="" class="img-fluid pb-3" width="350" height="268">
                            </div>
							<p class="lead">Imagio menyediakan produk dekorasi dengan teknologi digital printing.</p>
							<p>Tagline kami adalah “Print out of the box”.</p>
                            <p>Kami dapat mewujudkan gambar- gambar imajinasi Anda ke bahan- bahan keras
                                seperti kaca, keramik, kayu, acrylic, HPL.</p>
                            <div class="d-block d-md-none">
                                <img src="{{ asset('image/ceracqua/about-2.png') }}" alt="" class="img-fluid pb-3" width="350" height="268">
                            </div>
                            <p>Produk kami dapat digunakan untuk interior ruangan seperti hiasan dinding,
                                motif pada furniture, pembatas ruangan, wall panel, backing kitchen set dan sebagainya.</p>
                            <p>Imagio juga menyediakan jasa custom produk retail seperti
                                mencetak foto pada kaca, kayu, atau keramik, mencetak quotes
                                atau gambar pada kayu yang dapat digunakan sebagai souvenir acara istimewa Anda.</p>
{{--							<img src="img/arrow_about.png" alt="" class="arrow_1">--}}
						</div>
					</div>
					<div class="col-lg-5 pl-lg-5 text-center d-none d-lg-block">
							<img src="{{ asset('image/ceracqua/about-1.png') }}" alt="" class="img-fluid pb-3" width="350" height="268">
							<img src="{{ asset('image/ceracqua/about-2.png') }}" alt="" class="img-fluid" width="350" height="268">
					</div>
				</div>
{{--				<!-- /row -->--}}
{{--				<div class="row justify-content-center align-items-center">--}}
{{--					<div class="col-lg-5 pr-lg-5 text-center d-none d-lg-block">--}}
{{--							<img src="img/about_2.svg" alt="" class="img-fluid" width="350" height="268">--}}
{{--					</div>--}}
{{--					<div class="col-lg-5">--}}
{{--						<div class="box_about">--}}
{{--							<h2>Top Brands</h2>--}}
{{--							<p class="lead">Est falli invenire interpretaris id, magna libris sensibus mel id.</p>--}}
{{--							<p>Per eu nostrud feugiat. Et quo molestiae persecuti neglegentur. At zril definitionem mei, vel ei choro volumus. An tota nulla soluta has, ei nec essent audiam, te nisl dignissim vel. Ex velit audire perfecto pro, ei mei doming vivendo legendos. Cu sit magna zril, an odio delectus constituto vis. Vis ludus omnesque ne, est veri quaeque ad.</p>--}}
{{--							<img src="img/arrow_about.png" alt="" class="arrow_2">--}}
{{--						</div>--}}
{{--					</div>--}}
{{--				</div>--}}
{{--				<!-- /row -->--}}
{{--				<div class="row justify-content-center align-items-center ">--}}
{{--					<div class="col-lg-5">--}}
{{--						<div class="box_about">--}}
{{--							<h2>+5000 products</h2>--}}
{{--							<p class="lead">Est falli invenire interpretaris id, magna libris sensibus mel id.</p>--}}
{{--							<p>Per eu nostrud feugiat. Et quo molestiae persecuti neglegentur. At zril definitionem mei, vel ei choro volumus. An tota nulla soluta has, ei nec essent audiam, te nisl dignissim vel. Ex velit audire perfecto pro, ei mei doming vivendo legendos. Cu sit magna zril, an odio delectus constituto vis. Vis ludus omnesque ne, est veri quaeque ad.</p>--}}
{{--						</div>--}}

{{--					</div>--}}
{{--					<div class="col-lg-5 pl-lg-5 text-center d-none d-lg-block">--}}
{{--							<img src="img/about_3.svg" alt="" class="img-fluid" width="350" height="316">--}}
{{--					</div>--}}
{{--				</div>--}}
{{--				<!-- /row -->--}}
			</div>
			<!-- /container -->

{{--			<div class="bg_white">--}}
{{--				<div class="container margin_60_35">--}}
{{--					<div class="main_title">--}}
{{--						<h2>Why Choose Allaia</h2>--}}
{{--						<p>Cum doctus civibus efficiantur in imperdiet deterruisset.</p>--}}
{{--					</div>--}}
{{--					<div class="row">--}}
{{--						<div class="col-lg-4 col-md-6">--}}
{{--							<div class="box_feat">--}}
{{--								<i class="ti-medall-alt"></i>--}}
{{--								<h3>+ 1000 Customers</h3>--}}
{{--								<p>Id mea congue dictas, nec et summo mazim impedit. Vim te audiam impetus interpretaris, cum no alii option, cu sit mazim libris.</p>--}}
{{--							</div>--}}
{{--						</div>--}}
{{--						<div class="col-lg-4 col-md-6">--}}
{{--							<div class="box_feat">--}}
{{--								<i class="ti-help-alt"></i>--}}
{{--								<h3>H24 Support</h3>--}}
{{--								<p>Id mea congue dictas, nec et summo mazim impedit. Vim te audiam impetus interpretaris, cum no alii option, cu sit mazim libris. </p>--}}
{{--							</div>--}}
{{--						</div>--}}
{{--						<div class="col-lg-4 col-md-6">--}}
{{--							<div class="box_feat">--}}
{{--								<i class="ti-gift"></i>--}}
{{--								<h3>Great Sale Offers</h3>--}}
{{--								<p>Id mea congue dictas, nec et summo mazim impedit. Vim te audiam impetus interpretaris, cum no alii option, cu sit mazim libris.</p>--}}
{{--							</div>--}}
{{--						</div>--}}
{{--						<div class="col-lg-4 col-md-6">--}}
{{--							<div class="box_feat">--}}
{{--								<i class="ti-headphone-alt"></i>--}}
{{--								<h3>Help Direct Line</h3>--}}
{{--								<p>Id mea congue dictas, nec et summo mazim impedit. Vim te audiam impetus interpretaris, cum no alii option, cu sit mazim libris. </p>--}}
{{--							</div>--}}
{{--						</div>--}}
{{--						<div class="col-lg-4 col-md-6">--}}
{{--							<div class="box_feat">--}}
{{--								<i class="ti-wallet"></i>--}}
{{--								<h3>Secure Payments</h3>--}}
{{--								<p>Id mea congue dictas, nec et summo mazim impedit. Vim te audiam impetus interpretaris, cum no alii option, cu sit mazim libris.</p>--}}
{{--							</div>--}}
{{--						</div>--}}
{{--						<div class="col-lg-4 col-md-6">--}}
{{--							<div class="box_feat">--}}
{{--								<i class="ti-comments"></i>--}}
{{--								<h3>Support via Chat</h3>--}}
{{--								<p>Id mea congue dictas, nec et summo mazim impedit. Vim te audiam impetus interpretaris, cum no alii option, cu sit mazim libris. </p>--}}
{{--							</div>--}}
{{--						</div>--}}
{{--					</div>--}}
{{--					<!--/row-->--}}
{{--				</div>--}}
{{--			</div>--}}
{{--			<!-- /bg_white -->--}}

{{--		<div class="container margin_60">--}}
{{--        <div class="pb-3">--}}
{{--			<div class="main_title">--}}
{{--				<h2>Meet Our Staff</h2>--}}
{{--				<p>Cum doctus civibus efficiantur in imperdiet deterruisset.</p>--}}
{{--			</div>--}}
{{--			<div class="owl-carousel owl-theme carousel_centered">--}}
{{--				<div class="item">--}}
{{--					<a href="#0">--}}
{{--						<div class="title">--}}
{{--							<h4>Julia Holmes<em>CEO</em></h4>--}}
{{--						</div><img src="img/staff/1_carousel.jpg" alt="">--}}
{{--					</a>--}}
{{--				</div>--}}
{{--				<div class="item">--}}
{{--					<a href="#0">--}}
{{--						<div class="title">--}}
{{--							<h4>Lucas Smith<em>Marketing</em></h4>--}}
{{--						</div><img src="img/staff/2_carousel.jpg" alt="">--}}
{{--					</a>--}}
{{--				</div>--}}
{{--				<div class="item">--}}
{{--					<a href="#0">--}}
{{--						<div class="title">--}}
{{--							<h4>Paul Stephens<em>Business strategist</em></h4>--}}
{{--						</div><img src="img/staff/3_carousel.jpg" alt="">--}}
{{--					</a>--}}
{{--				</div>--}}
{{--				<div class="item">--}}
{{--					<a href="#0">--}}
{{--						<div class="title">--}}
{{--							<h4>Pablo Himenez<em>Customer Service</em></h4>--}}
{{--						</div><img src="img/staff/4_carousel.jpg" alt="">--}}
{{--					</a>--}}
{{--				</div>--}}
{{--				<div class="item">--}}
{{--					<a href="#0">--}}
{{--						<div class="title">--}}
{{--							<h4>Andrew Stuttgart<em>Admissions</em></h4>--}}
{{--						</div><img src="img/staff/5_carousel.jpg" alt="">--}}
{{--					</a>--}}
{{--				</div>--}}
{{--			</div>--}}
{{--			<!-- /carousel -->--}}
{{--		</div>--}}
		<!-- /container -->
	</main>
	<!--/main-->

@endsection

	<div id="toTop"></div><!-- Back to top button -->

