@extends('layouts.frontend')

@section('head_and_title')
    <meta name="description" content="Ceracacqua Project">
    <meta name="author" content="PT. Generasi Muda Gigih">
    <meta name="keywords"
          content="Ceracqua Imagi Kreasindo, Ceramic, Ceramic Decoration, Ceramic Digital Printing, Creative Industry">

    <title>PT Ceracqua Imagi Kreasindo</title>
@endsection



@section('content')
    <main>
	    <div class="top_banner version_2">
	        <div class="opacity-mask d-flex align-items-center" data-opacity-mask="rgba(0, 0, 0, 0)" style="background-color: rgba(0, 0, 0, 0);">
	            <div class="container">
	                <div class="d-flex justify-content-center">
	                    <h1>PROJECTS</h1>
	                </div>
	            </div>
	        </div>
            <img src="{{asset('img/bg_cat_shoes.jpg')}}" class="img-fluid" alt="">
	    </div>
	    <!-- /top_banner -->
	    <div id="stick_here"></div>
{{--	    <div class="toolbox elemento_stick">--}}
{{--	        <div class="container">--}}
{{--	            <ul class="clearfix">--}}
{{--	                <li>--}}
{{--	                    <div class="sort_select">--}}
{{--	                        <select name="sort" id="sort">--}}
{{--	                            <option value="popularity" selected="selected">Sort by popularity</option>--}}
{{--	                            <option value="rating">Sort by average rating</option>--}}
{{--	                            <option value="date">Sort by newness</option>--}}
{{--	                            <option value="price">Sort by price: low to high</option>--}}
{{--	                            <option value="price-desc">Sort by price: high to--}}
{{--	                        </select>--}}
{{--	                    </div>--}}
{{--	                </li>--}}
{{--	                <li>--}}
{{--	                    <a href="#0"><i class="ti-view-grid"></i></a>--}}
{{--	                    <a href="listing-row-1-sidebar-left.html"><i class="ti-view-list"></i></a>--}}
{{--	                </li>--}}
{{--	                <li>--}}
{{--	                    <a href="#0" class="open_filters">--}}
{{--	                        <i class="ti-filter"></i><span>Filters</span>--}}
{{--	                    </a>--}}
{{--	                </li>--}}
{{--	            </ul>--}}
{{--	        </div>--}}
{{--	    </div>--}}
	    <!-- /toolbox -->
	    <div class="container margin_30">
	        <div class="row">
                <div class="col"></div>
	            <div class="col-md-9">
                    <!-- /row_item -->
                    <!-- /row_item -->

                    @foreach($projects as $project)
                    <div class="row row_item pb-3">

                        <div class="col-sm-4">
                            <figure>
                                <a href="{{ route('frontend.project.show', ['id' => $project->id]) }}">
                                {{--	                            <span class="ribbon off">-30%</span>--}}
                                @php( $mainImage = \App\Models\ProjectImage::where('project_id',
                                $project->id)->where('is_main_image', 1)->first() )
                                <img src="{{ asset('storage/projects/'. $mainImage->path) }}" class="img-fluid lazy" alt="image project">
                                {{--	                            <div data-countdown="2019/05/15" class="countdown"></div>--}}
                                </a>
                            </figure>
                        </div>
                        <div class="col-sm-8 pt-3">
                            {{--	                        <div class="rating"><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star"></i></div>--}}
                            <a href="{{ route('frontend.project.show', ['id' => $project->id]) }}">
                                <h1>{{ $project->name }}</h1>
                            </a>
{{--                            <div style="display: inline-block;">--}}
{{--                                {{ $projects->links() }}--}}
{{--                            </div>--}}
                            <p>{{ $project->introduction }}</p>
                            {{--	                        <div class="price_box">--}}
                            {{--	                            <span class="new_price">$48.00</span>--}}
                            {{--	                            <span class="old_price">$60.00</span>--}}
                            {{--	                        </div>--}}
                            {{--	                        <ul>--}}
                            {{--	                            <li><a href="#0" class="btn_1">Add to cart</a></li>--}}
                            {{--	                            <li><a href="#0" class="btn_1 gray tooltip-1" data-toggle="tooltip" data-placement="top" title="Add to favorites"><i class="ti-heart"></i><span>Add to favorites</span></a></li>--}}
                            {{--	                            <li><a href="#0" class="btn_1 gray tooltip-1" data-toggle="tooltip" data-placement="top" title="Add to compare"><i class="ti-control-shuffle"></i><span>Add to compare</span></a></li>--}}
                            {{--	                        </ul>--}}
                        </div>
                    </div>
                    @endforeach
	            </div>
                <div class="col"></div>
                <div class="pagination__wrapper">
                    <ul class="pagination">
                        {{ $projects->links() }}
{{--                        <li><a href="#0" class="prev" title="previous page">&#10094;</a></li>--}}
{{--                        <li>--}}
{{--                            <a href="#0" class="active">1</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="#0">2</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="#0">3</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="#0">4</a>--}}
{{--                        </li>--}}
{{--                        <li><a href="#0" class="next" title="next page">&#10095;</a></li>--}}
                    </ul>
                </div>


	            <!-- /col -->
{{--	            <aside class="col-lg-3" id="sidebar_fixed">--}}
{{--	            </aside>--}}
	            <!-- /col -->
	        </div>
	        <!-- /row -->
	    </div>
	    <!-- /container -->
	</main>
{
@endsection

	<div id="toTop"></div><!-- Back to top button -->


